# LazyLoad
> HTML5/ES6探索之自定义元素
> [github地址](https://gitee.com/xuazheng/LazyLoad)访问源代码

#### 项目介绍
图片懒加载html5原生组件

> lazy-image组件，html5原生组件，无需依赖任何框架，拿来就用。

#### 安装教程

```bash
// install
npm i mem-lazyload -D
```

#### 使用说明

```bash
// html
<lazy-image src="http://xxx"></lazy-image>
<lazy-image src="http://xxx">
    <img class="mark" /> // 可以在图片上方添加其他布局比如打标
</lazy-image>

// 引入
import LazyLoad from 'mem-lazyload';

// 传递一个可滚动容器，默认window
let lazy = new LazyLoad(window);

// 当列表更新后
lazy.update();

```

#### 注意事项
由于强制采用es6 class类继承的语法，因此不能使用babel编译此文件。

#### 属性列表

##### src*
src存放图片的原链接，必传

##### init-src
init-src可以自定义加载图片前的占位图片，不传会使用默认图

##### active-class
自定义加载动画，默认为淡出效果（如果不想要动画，请随便传递一个class，不需要任何样式，如 no-animation）

#### 联系作者
> qq: 1131029395